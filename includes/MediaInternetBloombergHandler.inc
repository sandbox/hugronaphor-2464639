<?php

/**
 * @file
 * Extends the MediaInternetBaseHandler class for Bloomberg.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetBloombergHandler extends MediaInternetBaseHandler {

  /**
   * Parse given bloomberg url.
   */
  public function parse($embedCode) {
    // E.g: http://bbg.vidible.tv/#search/v/5530bdd2e4b00532ab3f5632
    // We are expecting just the video ID (5530bdd2e4b00532ab3f5632).
    if (ctype_xdigit($embedCode) && $this->validId($embedCode)) {
      return file_stream_wrapper_uri_normalize('bloomberg://v/' . $embedCode);
    }
  }

  /**
   * Determines if this handler should claim the item.
   */
  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  /**
   * Get the file object.
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    $video_data = $this->getVideoData();
    preg_match('~"name":"(.*)","videoUrls~Usmi', $video_data, $match);
    $title = !empty($match[1]) ? $match[1] : FALSE;

    // Try to default the file name to the video's title.
    if (empty($file->fid) && $title) {
      // Remove non-alphanumeric characters (e.g: 😍)
      $title = preg_replace('/[^a-z\d ]/i', '', check_plain($title));
      $file->filename = truncate_utf8($title, 255);
    }
    return $file;
  }

  /**
   * Returns information about the media.
   *
   * @return mixed
   *   JavaScript containing unordered data about video.
   */
  public function getVideoData() {
    $uri = $this->parse($this->embedCode);
    $video_id = arg(1, file_uri_target($uri));
    $oembed_url = _bloomberg_js_url($video_id, TRUE);
    $response = drupal_http_request($oembed_url);

    if (!isset($response->error)) {
      return $response->data;
    }

    throw new Exception("Error Processing Request. (Error: {$response->code}, {$response->error})");
  }

  /**
   * Check if a bloomberg video ID is valid.
   *
   * @return bool
   *   TRUE if the video ID is valid, or throws a
   *   MediaInternetValidationException otherwise.
   */
  static public function validId($id) {
    $url = _bloomberg_js_url($id, TRUE);
    $response = drupal_http_request($url, array('method' => 'HEAD'));

    if ($response->code == 401) {
      throw new MediaInternetValidationException('Seems like this Bloomberg video requires special permissions.');
    }
    elseif ($response->code != 200) {
      throw new MediaInternetValidationException('The Bloomberg video ID is invalid or the video was deleted.');
    }

    return TRUE;
  }

}
