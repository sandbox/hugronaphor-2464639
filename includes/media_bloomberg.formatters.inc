<?php

/**
 * @file
 * File formatters for bloomberg videos.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_bloomberg_file_formatter_info() {
  $formatters['media_bloomberg_video'] = array(
    'label' => t('Bloomberg Video'),
    'file types' => array('video'),
    'default settings' => array(),
    'view callback' => 'media_bloomberg_file_formatter_video_view',
    'settings callback' => 'media_bloomberg_file_formatter_video_settings',
    'mime types' => array('video/bloomberg'),
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_bloomberg_file_formatter_video_view($file, $display) {
  $scheme = file_uri_scheme($file->uri);
  $bloomberg_player_id = variable_get('bloomberg_player_id');

  // Check for file scheme and the Player Id to be available.
  if ($scheme == 'bloomberg' && $bloomberg_player_id) {
    $element = array(
      '#theme' => 'media_bloomberg_video',
      '#uri' => $file->uri,
      '#options' => array(),
    );
    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = array();

    $fields = array(
      'attributes',
    );

    foreach ($fields as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }

    return $element;
  }
}
