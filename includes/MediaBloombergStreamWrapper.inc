<?php

/**
 * @file
 * Extends the MediaReadOnlyStreamWrapper class for Bloomberg.
 */

/**
 * Implementation of MediaReadOnlyStreamWrapper.
 *
 * Create an instance like this:
 * $bloomberg = new MediaBloombergStreamWrapper('bloomberg://v/[video-id]');
 */
class MediaBloombergStreamWrapper extends MediaReadOnlyStreamWrapper {

  /**
   * Get the Bloomberg TimeType.
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'video/bloomberg';
  }
}
