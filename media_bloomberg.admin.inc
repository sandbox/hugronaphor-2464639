<?php

/**
 * @file
 * Admin page.
 */

/**
 * Implements hook_form().
 */
function bloomberg_admin_form() {
  $form['bloomberg_player_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Bloomberg Player ID'),
    '#default_value' => variable_get('bloomberg_player_id'),
    '#required' => TRUE,
  );

  $example = check_plain('<script type=\'text/javascript\' src="//delivery.vidible.tv/jsonp/pid=');
  $example .= '<strong>552e29c7e4b066203643f213/55082b4de4b021dfbc550aa4</strong>';
  $example .= check_plain('.js"></script>');

  $form['bloomberg_player_id_info'] = array('#markup' => t('Player ID represents the pid and .js file name. E.g:<br /> !eg', array('!eg' => $example)));

  return system_settings_form($form);
}
