<?php

/**
 * @file
 * Theme and preprocess functions for Media: Bloomberg.
 */

/**
 * Preprocess function for theme('media_bloomberg_video').
 */
function media_bloomberg_preprocess_media_bloomberg_video(&$variables) {

  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = $parts['v'];
  $file_object = file_uri_to_object($variables['uri']);

  // Add template variables (as we can't get any other video info).
  $variables['title'] = check_plain($file_object->filename);

  // Build the required player specific class.
  $bloomberg_player_id = variable_get('bloomberg_player_id');
  $variables['vdb_class'] = 'vdb_' . str_replace('/', '', $bloomberg_player_id);

  // Build the iframe URL with options query string.
  // E.g: src="//delivery.vidible.tv/jsonp/pid=552e29c7e4b066203643f213/vid=552ea5e7e4b0e229d86e5d1a/55082b4de4b021dfbc550aa4.js"
  $variables['url'] = _bloomberg_js_url($variables['video_id']);
}
