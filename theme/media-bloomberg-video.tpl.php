<?php
/**
 * @file
 * Template file for theme('media_bloomberg_video').
 *
 * Variables available:
 *  $uri - The media uri for the Bloomberg video (e.g. bloomberg://v/5530bdd2e4b00532ab3f5632).
 *  $video_id - The unique identifier of the Bloomberg video (e.g., 5530bdd2e4b00532ab3f5632).
 *  $vdb_class - A required class which is required to render the video.
 *  $url - The Bloomberg script src.
 */
?>

<div class="vdb_player <?php print $vdb_class; ?>">  
  <script type='text/javascript' src="<?php print $url; ?>"></script>
</div>
