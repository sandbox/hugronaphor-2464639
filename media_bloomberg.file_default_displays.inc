<?php

/**
 * @file
 * Default display configuration for the default file types.
 */

/**
 * Implements hook_file_default_displays().
 */
function media_bloomberg_file_default_displays() {
  $file_displays = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__default__media_bloomberg_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  // There are no settings which can be made on our side.
  // All configuration are coming from remote JS.
  $file_display->settings = array();
  $file_displays['video__default__media_bloomberg_video'] = $file_display;

  return $file_displays;
}
