CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Usage
 * Sponsors

INTRODUCTION
------------

Current Maintainers:

 * Cornel Andreev <http://drupal.org/user/2333992>

Media: Bloomberg adds Bloomberg as a supported media provider.

REQUIREMENTS
------------

Dependencies:
 * Media Internet - A submodule of the Media module.

INSTALLATION
------------


Media: Bloomberg can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).

USAGE
-----

Media: Bloomberg integrates the Bloomberg video-sharing service with the Media module to
allow users to add and manage Bloomberg videos as any other piece of media.

Internet media can be added on the Web tab of the Add file page (file/add/web).
With Media: Bloomberg enabled, users can add a Bloomberg video by entering its ID
(e.g. 5530d2afe4b0b0085a5a731b from http://bbg.vidible.tv/#search/v/5530d2afe4b0b0085a5a731b

Also you will need to setup the Player ID.
Please visit /admin/config/media/bloomberg and add your Bloomberg Video ID.

SPONSORS
--------

This module was kindly sponsored by www.dennis.co.uk.
